<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Vývoj CSS</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__html"><h1 id="vývoj-css">Vývoj CSS</h1>
<p>Tato brožura pojednává o základní orientaci sdílených komponent a praktikami pro psaní tříd. Dbá se především na znovupoužitelnost a jednoduchost.</p>
<h2 id="komponenta">Komponenta</h2>
<p>Komponenta do sebe slučuje <code>Závislé</code> a <code>Nezávislé</code> třídy. Za účelem vytvoření znovupoužitelného prvku. Typickým příkladem je třeba Article item (položka článku ve výpise).</p>
<h2 id="page">Page</h2>
<p>Page do sebe slučuje minimálně 2 a více komponent. Typickým příkladem je třeba Profile page.</p>
<h2 id="definice-třídy">Definice třídy</h2>
<p>Třídy dělíme na <code>Závislé</code> a <code>Nezávislé</code>.</p>
<ul>
<li><code>Nezávislé</code> třídy mohou být použity kdekoliv v projektu a <strong>nevyžadují</strong> k sobě žádnou další návaznost.</li>
<li><code>Závislé</code> třídy jsou použity v Komponentách a <strong>je nutné</strong> je využít v souvislosti s jinými třídami. Typickým příkladem je definice Rodiče.</li>
</ul>
<h3 id="nezávislé-třídy">Nezávislé třídy</h3>
<p>Nezávislé třídy nevyžadují další návaznost, ale mohou ji použít. Typickým příkladem je třeba Button.</p>
<pre class=" language-html"><code class="prism  language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
</code></pre>
<p>Třída <code>.btn</code> nám vytvoří krásné a znovupoužitelné tlačítko. Je napsána tak, aby fungovala na 100% samostatně, ale může přijímat další <code>class parametry</code>.</p>
<pre class=" language-html"><code class="prism  language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-danger<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
</code></pre>
<p>Parametr musí začínat vždy prefixem: hlavní třída + pomlčka (tj <code>btn-</code>).</p>
<p>Parametry mohou být definovány v dokumentaci. Zároveň mohou být uzavřeny do svých vlastních skupin, u kterých platí, že lze použít právě jen jednu z nich. V případě našeho tlačítka by to mohli být třeba tyto skupiny:</p>
<ul>
<li>danger, success, warning</li>
<li>mini, small, large</li>
<li>…</li>
</ul>
<blockquote>
<p>Využili jsme skvělý nástroj <a href="https://stackedit.io/">StackEdit</a>. Děkujeme!</p>
</blockquote>
</div>
</body>

</html>
